<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\Enumeration\ClientTypeEnumeration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $user = new User();
        $user
            ->setUsername('morty')
            ->setEmail('morty@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user);
        $this->addReference('user', $user);
        $manager->flush();

        $admin = new User();
        $admin
            ->setUsername('admin')
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $this->addReference('admin', $admin);
        $manager->flush();
    }
}

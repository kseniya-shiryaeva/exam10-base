<?php


// src/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    private $newPass;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }


}